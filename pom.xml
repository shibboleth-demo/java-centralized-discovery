<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>net.shibboleth</groupId>
        <artifactId>parent-v2</artifactId>
        <version>TRUNK-SNAPSHOT</version>
    </parent>

    <groupId>edu.internet2.middleware</groupId>
    <artifactId>shibboleth-discovery-service</artifactId>
    <version>1.2.1</version>
    <!-- We bundle as a jar here, the installer creates the WAR -->
    <packaging>jar</packaging>

    <name>Shibboleth Discovery Service</name>
    <description>
        The Shibboleth Discovery Service is used by a Shibboleth Service Provider to determine the correct 
        Identity Provider for a user.  It may be deployed centrally (e.g. at a federation level) or co-located
        with a Service Provider.
    </description>

    <properties>
        <svn.relative.location>java-centralized-discovery</svn.relative.location>
    </properties>

    <repositories>
        <repository>
            <id>shib-release</id>
            <url>https://build.shibboleth.net/nexus/content/groups/public</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>shib-snapshot</id>
            <url>https://build.shibboleth.net/nexus/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
    </repositories>

    <dependencies>
        <!-- Compile dependencies -->
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <version>1.3</version>
        </dependency>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>1.6.2</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
        </dependency>
        <dependency>
            <groupId>org.opensaml</groupId>
            <artifactId>opensaml</artifactId>
            <version>2.5.3</version>
        </dependency>
        <dependency>
            <groupId>org.apache.struts</groupId>
            <artifactId>struts-core</artifactId>
            <version>1.3.10</version>
            <exclusions>
                <exclusion>
                    <groupId>commons-logging</groupId>
                    <artifactId>commons-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.struts</groupId>
            <artifactId>struts-taglib</artifactId>
            <version>1.3.10</version>
            <optional>false</optional>
        </dependency>

        <!-- Provided dependencies -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
        </dependency>

        <!-- Runtime dependencies -->
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant-nodeps</artifactId>
            <version>1.7.1</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>ant-contrib</groupId>
            <artifactId>ant-contrib</artifactId>
            <version>1.0b2</version>
            <scope>runtime</scope>
            <exclusions>
                <exclusion>
                    <groupId>ant</groupId>
                    <artifactId>ant</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>edu.internet2.middleware</groupId>
            <artifactId>ant-extensions</artifactId>
            <version>13Apr2008</version>
            <scope>runtime</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.bouncycastle</groupId>
                    <artifactId>bcprov-jdk14</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>${xerces.groupId}</groupId>
            <artifactId>xml-apis</artifactId>
        </dependency>
        <dependency>
            <groupId>${xerces.groupId}</groupId>
            <artifactId>xercesImpl</artifactId>
        </dependency>
        <dependency>
            <groupId>${xerces.groupId}</groupId>
            <artifactId>serializer</artifactId>
        </dependency>
        <dependency>
            <groupId>xml-resolver</groupId>
            <artifactId>xml-resolver</artifactId>
        </dependency>
        <dependency>
            <groupId>${xalan.groupId}</groupId>
            <artifactId>xalan</artifactId>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>3.8.1</version>
            <scope>test</scope>
        </dependency>
        
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <index>true</index>
                        <manifestEntries>
                            <Main-Class>edu.internet2.middleware.shibboleth.wayf.Version</Main-Class>
                        </manifestEntries>
                        <manifestSections>
                            <manifestSection>
                                <name>edu/internet2/middleware/shibboleth/wayf/</name>
                                <manifestEntries>
                                    <Implementation-Title>${project.artifactId}</Implementation-Title>
                                    <Implementation-Version>${project.version}</Implementation-Version>
                                    <Implementation-Vendor>shibboleth.internet2.edu</Implementation-Vendor>
                                </manifestEntries>
                            </manifestSection>
                        </manifestSections>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>
    
    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-assembly-plugin</artifactId>
                        <configuration>
                            <descriptors>
                                <descriptor>src/main/assembly/bin.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>
